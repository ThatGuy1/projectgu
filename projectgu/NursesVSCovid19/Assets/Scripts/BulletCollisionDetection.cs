﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletCollisionDetection : MonoBehaviour
{

    public int hazardDamage;
    private void OnCollisionEnter2D(Collision2D collision)
    { //what was collied with?
        Collider2D objectWeCollideWith = collision.collider;

        /*------------------------------------------------------------------
                           sarah notes
        --------------------------------------------------------------------
        //get the playerhealth script attached to that object (if there is one) - player detection
            PlayerHealth player = objectWeCollideWith.GetComponent<PlayerHealth>();

        //create a variable called player and into that variable put in the playerhealth component that is attached to the object we collide with

            if (player != null) //check if we actually found a player health scipt on the object we collide with
                    //this if statement is true if the player variable is NOT null (aka empty)
            {
             //this means there was a PlayerHealth script attached to the object we bumped into
             // which mean this object is indeed the player

             //therefore perform our action
                player.Kill(); //variable(player) containing the function(kill)
                               //this will go to the player variable and use the kill function
                               //currently nothing will happen if the player and the "spike" dont have a 2d collider (in unity)
                               //like circle for spheres(2d)
            }

        ------------------------------------------------------------------
                    end of sarah notes
        ------------------------------------------------------------------*/

/*        //if its the player?

            //get the PlayerHealth Script attached to the object (if there is one?)
                PlayerHealth player = objectWeCollideWith.GetComponent<PlayerHealth>();

            //check if there actually is a PlayerHealth script attached to the object we collided with
            // if there is that object is the player :D
                if (player != null)
                {
                 //therefore we perform our action
                     player.DamagePlayer(-hazardDamage); //this will remove "PlayerDamaged" value from the player health
                     Destroy(gameObject); //destroy the bullet being used in this interaction


                }
*/        //end of player check

        //if its an enemy?
                //get the EnemyHealth script attached to the object (if there is one?)
                    EnemyHealth enemy = objectWeCollideWith.GetComponent<EnemyHealth>();

        //check if there actually is a EnemyHealth scrupt attached to the object we collided with
        // if there is that object is the Enemy :D
        if (enemy != null)
        {
            //therefore we perform out action
            enemy.DamageEnemy(); // this will remove "EnemyDamaged" value from the enemy health
            //Destroy(gameObject);//destroy the bullet being used in this interaction
            Debug.Log("this spawned");
        }

        //end of enemy check

    }
    

//------------------------------------------------------------------------------------------------
//timed bullets

    //unity editor variable
    public float goalTime; // number of seconds the counter will count for (max time on the timer)


    //private variable (used internally in the script)
        private float startTime; // time stamp that our timer is starting at. (time stamp = seconds since game started)
    void Start() // Start is called before the first frame update
    {
        //time.time = current time stamp ( time since the game started)
        startTime = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        //Declare a variable to contain our end timestamp to be used for comparisons
        float endTime;
        // the end timestamp is just the start timestamp plus how long the object should live (timer max)
        endTime = startTime + goalTime;
        //check if our current time is greater or equal to our end time
            //if it is perform our action
        if (Time.time >= endTime)
        {
            //action : destroy this object
                    Destroy(gameObject);
                    Debug.Log("Byebye bullet");
        }
    }


    //end of timed bullets
    //---------------------------------------------------------------------------------------------------


    /*
      ----------------------------------------
            stuff to figure out / place
      ----------------------------------------

        if its another bullet?
           do nothing? destroy both?
           "Destroy(gameObject);" - this will the object(the bullet) this script is attached to.

        if its a wall/ground?
        this was sorted by adding a timer - like a self destruct of the bullet.

        if its nothing?
            how long does the bullet travel for? - for the amount of time specified as the "goalTime" then self destruct bullet.
    */
}
