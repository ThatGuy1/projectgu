﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelLoad : MonoBehaviour
{
    public string LevelToLoad;
    void Update()
    {

    }

    private void OnCollisionEnter2D(Collision2D collision)
    { //what was collied with?
        Collider2D objectWeCollideWith = collision.collider;

            //if its the player?

                //get the PlayerHealth Script attached to the object (if there is one?)
                    PlayerHealth player = objectWeCollideWith.GetComponent<PlayerHealth>();
                
                //check if there actually is a PlayerHealth script attached to the object we collided with
                    // if there is, that object is the player :D
                        if (player != null)
                        {
                            //therefore we perform our action
                                LevelExit(); //this will call the 2nd level to load in via the SceneManager in Unity
                        }
            //end of player check
    }

    public void LevelExit()
    {
        //Destroy(gameObject);
        Debug.Log("Level Exit called");

        SceneManager.LoadScene("WinningScene"); //this will load the scene with the "identified name".

        //this will load the scene on top of any others already loaded - may use for a menu system.
        // ,LoadSceneMode.Additive);
        Debug.Log("SceneManager has changed scene to 'Level 2' ");
    }

    public void LevelOne()
    {
        Debug.Log("LevelOne Function Called");
        SceneManager.LoadScene(LevelToLoad); //this will load the scene "Level 1".
        Debug.Log("ScenemManager has changed scene to 'Level 1' ");
    }

 // OPTIONAL CODE TO LOAD LEVEL IF CERTAIN SCORE IS REACHED  
    /*    //public variable
            public Text scoreDisplay;
            public bool shouldReset = false;
            public int winningScore;
            public string winningScene;
            //private variables
            private static int scoreValue = 0;

            //called by unity the frame after this object is created
            private void Start()
            {
                //check if we should be resetting the score this scene
                if (shouldReset == true)
                {
                    //reset the score value back to 0
                    scoreValue = 0;
                }

                //update the display of the score based on the numerical value
                scoreDisplay.text = scoreValue.ToString();
            }

            //function to add to the player's score
            // NOT automatically called to unity - we need to call it ourselves in our code.
            public void AddScore(int toAdd)
            {
                // update the numerical value of the score (action 1)
                scoreValue = scoreValue + toAdd;

                //update the display of the score based on the numerical value
                scoreDisplay.text = scoreValue.ToString();

                //check if score is bigger than win score
                if (scoreValue >= winningScore)
                {
                    //our score is a winning score! load win scene!
                    SceneManager.LoadScene(winningScene);
                }
            }
    */
}
