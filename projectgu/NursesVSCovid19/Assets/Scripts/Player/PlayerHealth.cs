﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class PlayerHealth : MonoBehaviour
{
    public int PlayerHP; 
    public int PlayerDamaged;
    public int startingHealth = 10; // current player health value

    private void Awake()
    {PlayerHP = Mathf.Clamp(PlayerHP, 0, startingHealth);
        PlayerHP = startingHealth;
    }
    void Update()
    {
        if (PlayerHP < 0)
        {
            Kill();
        }
    }

    public void DamagePlayer(int changeAmount)// is this health being changed?
    {
        PlayerHP = PlayerHP + changeAmount; // the current health is changed by whatever value change amount is, weather thats positive or negative.
        Debug.Log("ah - u have been hit :(");

        
    }

  public void Kill()
  {
        //Destroy(gameObject);
        Debug.Log("player has died but not removed gameObject from scene");

        SceneManager.LoadScene("GameOver"); //this will load the scene with the "identified name".

        //this will load the scene on top of any others already loaded - may use for a menu system.
        // ,LoadSceneMode.Additive);
        Debug.Log("SceneManager has changed scene to 'GameOver' ");
    }

}
