﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFire : MonoBehaviour
{
    // Unity editor variable
        public GameObject Player; //find the player
        public GameObject BulletPrefab; // prefab for bullet cloning
        public Vector2 BulletVelocity;// set the speed of the bullet
        public Vector3 Offset; // set the offset for how close to the player's transform it should spawn the bullet
        public float bulletBetweenDistance; // the distance to the end point from where the bullet currently is
        public Transform BulletGoal; // the end point of the bullet
    private List<GameObject> InGameBullets;
    public int StartamnBulletsOnScreen = 0;
    public void Start()
    {
      Player  = GameObject.FindGameObjectWithTag("Player"); //set the "Player" object as the unity object tagged player (so you have the same info)

        InGameBullets = new List<GameObject>();
        for (int i = 0; i < StartamnBulletsOnScreen; i++)
        {
            FireBullet();

        }
    }

    // ACTION: Fire a Bullet
    public void FireBullet()
    {
        Debug.Log("This was called :)");
        // clone the Bullet
            GameObject clonedBullet; //declare a variable to hold the cloned object
            clonedBullet = Instantiate(BulletPrefab) as GameObject; //use the instantiate to clone the Bullet and keept the result in our variable
            clonedBullet.transform.position = Player.transform.position + Offset;//position the Bullet on the player + optional : add an offest (use a public variable)
        // at this point the bullet is spawned at the player's position (+ offset) but does nothing
        InGameBullets.Add(clonedBullet); // add a new bullet to the list (required to destroy)

        //fire it in a direction
            Rigidbody2D BulletRigidbody; //declare a variable to hold the cloned object's rigidbody
            BulletRigidbody = clonedBullet.GetComponent<Rigidbody2D>(); //get the rigidbody from our cloned Bullet and store it
            BulletRigidbody.velocity = BulletVelocity; //set the velocity(speed) on the rigidbody to the editor setting
            
            BulletVelocity.x = BulletVelocity.x + 1; //this will make each time the bullet spawns, faster along the x axis? why is it getting faster?

        // the bullet now fires from the player(+ offset) position  and moves at the velocity told by unity (minus forces like friction till it hits 0) but doesn't despawn

        //-----------------------------------------------------------------------------------------------------------------------------------------------------------
        /*done by BulletCollisionDetection
        - if bullet collides with enemy, checks enemy health if  =< 0 then kill enemy
        */
        
        /* still to be added to BulletCollisionDetection
        + add score to total
        */


        // getting the bullet to despawn after x distance is reached. if collide what are u colliding with?
        bulletBetweenDistance = (BulletGoal.position /*end target(currently the player*/ - clonedBullet.transform.position).magnitude;/*bullet current position*/ //.normalized // would make it always = 1;
        
//        if (clonedBullet.GetComponent<Rigidbody2D>().transform.position.x <= -10) //this will delete the bullet after a certain distance
//        { Destroy(InGameBullets[0]);
//            InGameBullets.RemoveAt(0);
//        }

     //   if (bulletBetweenDistance > 1) // this will delete the bullet if its less than a certain distance
     //      Destroy(BulletRigidbody.gameObject);
            //Score.AddScore(); // Enemyvalue = the score for each enemy type - in hard numbers (gdd) - this would end up in a seperate code to be referenced.


    }
    /* collision for the bullet is handeld by BulletCollisionDetection(); */
}
