﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovementEdit : MonoBehaviour
{
    private Rigidbody2D controller;
    private Vector3 moveVector;
    public float speed = 50.0f; //this is the speed variable (mentioned in controls)
    
    void Start()    // Start is called before the first frame update
    {
        controller = GetComponent<Rigidbody2D>(); //goes to unity to find the rigidbody2d used on this object
    }

    void Update()    // Update is called once per frame
    {
        //----------controls---------------------------------------------------------------------------------
        //x - Horizontal (Left and Right)
        moveVector.x = Input.GetAxisRaw("Horizontal") * speed; // move around using a/left and d/right

        //Y - Vertical - Up and Down 
        moveVector.y = Input.GetAxisRaw("Vertical") * speed; // move around using w/up and s/down

        //camera follow me---------------------------------------------------------------------------
        controller.velocity = moveVector * Time.deltaTime;
        //controller.Move(moveVector * Time.deltaTime);//new version 1.1
        
    }
}
