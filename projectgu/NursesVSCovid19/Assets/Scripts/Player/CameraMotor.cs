﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMotor : MonoBehaviour
{
    public Rigidbody2D controller;
    public Transform lookAt;
    public Vector3 startOffset;
    public Vector3 moveVector;
    void Start()    // Start is called before the first frame update
    {
        lookAt = GameObject.FindGameObjectWithTag("Player").transform;
        startOffset = transform.position - lookAt.position;

    }

    void Update()    // Update is called once per frame
    {
        //camera follow me---------------------------------------------------------------------------
            controller.velocity = moveVector * Time.deltaTime;
                //controller.Move(moveVector * Time.deltaTime);//new version 1.1

        moveVector = lookAt.position + startOffset;
        transform.position = lookAt.position + startOffset; //tells the camera to follow the look at position whilst keeping the starting distance away from the look at position

    }
}
