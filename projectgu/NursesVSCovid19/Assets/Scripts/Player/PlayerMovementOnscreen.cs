﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovementOnscreen : MonoBehaviour
{
    //This was removed as i got "PlayerMovementEdit" working on 13.04.2021
    // this was a version that had onscreen buttons for mouse controls

    /*
     Functions, class names (scripts) should be capitalised (PascalCase), LikeThis.
     Class-wide variables, local variables, and function parameters, should be camelCase, likeThis.
    */

    public float movementForce;
    public Rigidbody2D rb;
        
    public void moveUp()
    {
     rb.AddForce(Vector2.up * movementForce);
    }
        
    public void moveDown()
    {
     rb.AddForce(Vector2.down * movementForce);
    }

    public void moveRight()
    {
     rb.AddForce(Vector2.right * movementForce);
    }

    public void moveLeft()
    {
     rb.AddForce(Vector2.left * movementForce);
    }
}
