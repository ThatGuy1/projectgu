﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : MonoBehaviour
{
    public int EnemyHP = 1;
    public int BossHP = 1;
    public int EnemyDamaged = 1;
    public int HP;
    public string EnemyType;
    void Update()
    {   if (EnemyHP < 0)
        {
            Kill();
        }
    }
    public void DamageEnemy()
    {
        EnemyHP = EnemyHP - EnemyDamaged;
        Debug.Log("OW! stop hitting me player :( ");
        /*    if (EnemyType = "Boss")
              HP = BossHP;
            else
              HP = EnemyHP;
        */
    }

    public void Kill()
    {
        Destroy(gameObject);
        //check if the enemy has the "ScoreKill" component
            ScoreKill scoreKillScript = GetComponent<ScoreKill>();
            if (scoreKillScript != null)
            {
                //this mean our enemy has a score value
                Score playerScore = FindObjectOfType<Score>();

                //add the value of the scoreKillScript
                playerScore.AddScore(scoreKillScript.killValue);
            }

        Debug.Log("Here lies a dead enemy");
    }

}
