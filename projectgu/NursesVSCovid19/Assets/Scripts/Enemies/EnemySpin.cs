﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpin : MonoBehaviour
{   // this code was supposed to create and store a variable for the attached objects rigid body to spin/rotate for a visual.
    private Rigidbody2D controller;
    private Vector3 moveVector;
    public float speed = 50.0f; //this is the speed variable (mentioned in controls)

    void Start()    // Start is called before the first frame update
    {
        controller = GetComponent<Rigidbody2D>(); //goes to unity to find the rigidbody2d used on this object
        
    }

    void Update()    // Update is called once per frame
    {
        controller.rotation = controller.rotation + speed;
    }
}
