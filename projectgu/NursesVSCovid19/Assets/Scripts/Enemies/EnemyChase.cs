﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyChase : MonoBehaviour
{
    // ------------------------------------------------
    // Public variables, visible in Unity Inspector
    // Use these for settings for your script
    // that can be changed easily
    // ------------------------------------------------
    public float forceStrength;     // How fast we move
    public Transform target;        // The thing you want to chase

//public Vector2 EnemySightDistance = ; // Tells the enemy how far it can see - if outside of it, it will carry on what it's doing
                                        //-if inside of it will go towards the player.

    // ------------------------------------------------
    // Private variables, NOT visible in the Inspector
    // Use these for tracking data while the game
    // is running
    // ------------------------------------------------
    private Rigidbody2D ourRigidbody;   // The rigidbody attached to this object


    // ------------------------------------------------
    // Awake is called when the script is loaded
    // ------------------------------------------------
    void Awake()
    {
        // Get the rigidbody that we'll be using for movement
        ourRigidbody = GetComponent<Rigidbody2D>();
    }


    // ------------------------------------------------
    // Update is called once per frame
    // ------------------------------------------------
    void Update()
    {
        // Move in the direction of our target

            // Get the direction
                // Subtract the current position from the target position to get a distance vector
                // Normalise changes it to be length 1, so we can then multiply it by our speed / force
                    Vector2 direction = ((Vector2)target.position - (Vector2)transform.position).normalized;

                // Move in the correct direction with the set force strength
                    ourRigidbody.AddForce(direction * forceStrength);
        //if (EnemySightDistance - direction < EnemySightDistance);
            float distance = ((Vector2)target.position - (Vector2)transform.position).magnitude;

     /*       //if its the player?
                //get the PlayerHealth Script attached to the object (if there is one?)
                    PlayerHealth player = objectWeCollideWith.GetComponent<PlayerHealth>();

                //check if there actually is a PlayerHealth script attached to the object we collided with
                // if there is that object is the player :D
                    if (player = null)
                    {
                    }
      */      //end of player check
    }

    // //figuring out the variables    Distance/SightDistance/EnemyPosition/PlayerPosition
    public float Distance;
            public float SightDistance;
            public Vector2 EnemyPosition;
            public Vector2 PlayerPosition;
            public float SightValue;
            public float VisibleDistance;
    private void OnCollisionEnter2D(Collision2D collision)
    { //what was collied with?
        Collider2D objectWeCollideWith = collision.collider;

        //sight distance(enemy.position + value) // the enemy's current position + the area they can see to.
//        ----------------------------------------------------------------------------------------------------
        //enemy.position (find the enemy - they have the EnemyHealth scripts so use an if statement)
        //if its an enemy?
        //get the EnemyHealth script attached to the object (if there is one?)
        EnemyHealth enemy = objectWeCollideWith.GetComponent<EnemyHealth>();
        //EnemySight enemySight = objectWeCollideWith.GetComponent<EnemySight>();
        //check if there actually is a EnemyHealth scrupt attached to the object we collided with
        // if there is that object is the Enemy :D
        if (enemy != null)
        {
            //therefore we perform out action
            
            EnemyPosition = enemy.transform.position; // get the position of the enemy (x,y)
            SightDistance = (EnemyPosition).magnitude + SightValue; // measure a line between the enemy (x,y) and the edge it can see to and store the value as SightDistance
        }

        //end of enemy check
//        ----------------------------------------------------------------------------------------------------
                     //player.position(find the player - they have the PlayerHealth script so use an if statement)
                     //if its the player?

                     //get the PlayerHealth Script attached to the object (if there is one?)
                     PlayerHealth player = objectWeCollideWith.GetComponent<PlayerHealth>();

        //check if there actually is a PlayerHealth scrupt attached to the object we collided with
        // if there is that object is the player :D
        if (player != null)
        {
            //therefore we perform our action
            PlayerPosition = player.transform.position;
        }
        //end of player check
//        ----------------------------------------------------------------------------------------------------
            Distance = ((EnemyPosition).magnitude - (PlayerPosition).magnitude); // measure a line bewtween the player and enemy
            VisibleDistance = SightDistance - Distance; // take the Sight away from the real distance - so can the enemy see them?
        if (VisibleDistance < 10) // if visibleDistance is less than the assigned value enter if statement 
        {
            //enemySight.Test();

            //else
            //i dont care where u are.
        }
    }
}
/*
public class EnemyDistanceDecision : MonoBehaviour
{   public float distanceForDecision;   // How close must the player be to change behaviour
    public Transform target;            // The thing you want to measure distance from
    private EnemyPatrol patrolBehaviour;
    private EnemyChase chaseBehaviour;
    
    void Awake()
    {
        // Get the EnemyPatrol that we'll be using for AI
            patrolBehaviour = GetComponent<EnemyPatrol>();
        // Get the EnemyPatrol that we'll be using for AI
            chaseBehaviour = GetComponent<EnemyChase>();

        // Turn off the chase behaviour to start with:
            chaseBehaviour.enabled = false;
    }

    void Update()
    {
        // How far away are we from the target?
            float distance = ((Vector2)target.position - (Vector2)transform.position).magnitude;

        // If we are closer to our target than our minimum distance...
            if (distance <= distanceForDecision)
            {
                // Disable patrol and enable chasing
                    patrolBehaviour.enabled = false;
                    chaseBehaviour.enabled = true;
            }
            else
            {
                // Enable patrol and disable chasing
                    patrolBehaviour.enabled = true;
                    chaseBehaviour.enabled = false;
            }

    }
}
*/