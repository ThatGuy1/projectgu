﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamagePlayer : MonoBehaviour
{
    public int hazardDamage;
    private void OnCollisionEnter2D(Collision2D collision)
    { //what was collied with?
        Collider2D objectWeCollideWith = collision.collider;

        
            //if its the player?

            //get the PlayerHealth Script attached to the object (if there is one?)
            PlayerHealth player = objectWeCollideWith.GetComponent<PlayerHealth>();

            //check if there actually is a PlayerHealth script attached to the object we collided with
            // if there is that object is the player :D
                if (player != null)
                {
                 //therefore we perform our action
                     player.DamagePlayer(-hazardDamage); //this will remove "PlayerDamaged" value from the player health
                     //destroy the bullet being used in this interaction


                }
        //end of player check
    }
}
