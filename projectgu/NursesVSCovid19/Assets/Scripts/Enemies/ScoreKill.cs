﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreKill : MonoBehaviour
{
    // public variable
    public int killValue = 1; // the value added each time an enemy is killed to the score
    public int BossValue = 10; //(then specify the amount)
    public int StandardValue = 1; //(then specify the amount - these can be adapted by telling unity what enemy it is in the inspector, but requires a base value)
    public bool Boss =false;

    /* if the enemy is of a specific type it should have a different value
       //so a boss isn't the same as killing a general enemy
       //this can be done by assigning different values to each case and asking which case is appropriate at the time
       //so a boss could have the kill value = 10, whilst a standard enemy could = 1
   */
    void Update()
    {
        if (Boss == true)
        {
            killValue = BossValue;
        }
        else
        {
            killValue = StandardValue;
        }
    }



}
