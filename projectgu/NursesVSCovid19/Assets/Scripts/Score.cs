﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

//   SETS THE SCORE ATTACHED IN UNITY TO THE CURRENT VALUE (the other version calling the winning scene is in LevelLoad if required)
public class Score : MonoBehaviour
{
    //public variable
    public Text ScoreDisplay;
    public bool shouldReset = false;
    //private variables
    private static int scoreValue = 0;

    //called by unity the frame after this object is created
    private void Start()
    {
        //check if we should be resetting the score this scene
        if (shouldReset == true)
        {
            //reset the score value back to 0
            scoreValue = 0;
        }

        //update the display of the score based on the numerical value
        ScoreDisplay.text = scoreValue.ToString();
    }

    //function to add to the player's score
    // NOT automatically called to unity - we need to call it ourselves in our code.
    public void AddScore(int toAdd)
    {
        // update the numerical value of the score (action 1)
        scoreValue = scoreValue + toAdd;
        Debug.Log("the scorevalue has been added to");
        //update the display of the score based on the numerical value
        ScoreDisplay.text = scoreValue.ToString();
    }
}